import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { GateComponent } from './gate/gate.component';
import { DirasaComponent } from './dirasa/dirasa.component';
import { WeareComponent } from './weare/weare.component';
import { EnvComponent } from './env/env.component';
import { AcceptanceComponent } from './acceptance/acceptance.component';
import { ProgrammeComponent } from './programme/programme.component';
import { ContactComponent } from './contact/contact.component';

const routes: Routes = [
  { path: '', component: HomeComponent },

  { path: 'login', component: LoginComponent },

  { path: 'gate', component: GateComponent },
  { path: 'dirasa' , component:DirasaComponent},
  {path:'weare', component:WeareComponent},
  {path:'env', component:EnvComponent},
  {path:'acceptance',component:AcceptanceComponent},
  {path:'programme',component:ProgrammeComponent},
  {path:'contact', component:ContactComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { GateComponent } from './gate/gate.component';
import { DirasaComponent } from './dirasa/dirasa.component';
import { WeareComponent } from './weare/weare.component';
import { HeaderComponent } from './core/header/header.component';
import { FooterComponent } from './core/footer/footer.component';
import { RouterModule } from '@angular/router';
import { EnvComponent } from './env/env.component';
import { AcceptanceComponent } from './acceptance/acceptance.component';
import { ProgrammeComponent } from './programme/programme.component';
import { ContactComponent } from './contact/contact.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    GateComponent,
    DirasaComponent,
    WeareComponent,
    HeaderComponent,
    FooterComponent,
    EnvComponent,
    AcceptanceComponent,
    ProgrammeComponent,
    ContactComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DirasaComponent } from './dirasa.component';

describe('DirasaComponent', () => {
  let component: DirasaComponent;
  let fixture: ComponentFixture<DirasaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DirasaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DirasaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
